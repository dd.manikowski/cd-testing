# cd-testing

That's just a simple project for checking capabilities of Gitlab CI/CD.

GitHub actions wasn't exactly as mature as I thought. Some things like workflow serialization, throttling or approvals are unfortunately missing.
Moved back to Jenkins but am also curious if Gitlab is the place to go.
